#ifndef Path_h__
#define Path_h__

#include <vector>
#include "AStarNode.h"

class Path
{
public:
	Path(void);
	~Path(void);
		
	int getLength();

	void addNode(AStarNode* n);

	/*`Path` filling modifier. Interpolates between non contiguous nodes along a `path`
	 to build a fully continuous `path`. This maybe useful when using search algorithms such as Jump Point Search.*/
	void fill();

	/*`Path` compression modifier. Given a `path`, eliminates useless nodes to return a lighter `path` 
	consisting of straight moves. Does the opposite of @{Path:fill}*/
	void filter();

	Path* clone();

	bool isEqualTo(Path* p2);

	void reverse();

	void append(Path* p);


public:
	std::vector<AStarNode*> _nodes;
	
};

#endif // Path_h__
