#ifndef ThataStar_h__
#define ThataStar_h__
#include "AStar.h"

class ThataStar:public AStar
{
public:
	ThataStar(void);
	~ThataStar(void);

	bool lineOfSight(AStarNode* n,AStarNode* neighbour,PathFinder* finder); 

	void computeCost(AStarNode* node,AStarNode* neighbour,PathFinder* finder);

};

#endif // ThataStar_h__
